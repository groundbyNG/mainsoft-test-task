## Getting Started

1. Clone this repo, `git clone https://groundbyNG@bitbucket.org/groundbyNG/mainsoft-test-task.git <your project name>`
2. Go to project's root directory, `cd <your project name>`
3. Run `npm install` to install dependencies
4. Start the packager with `npm start`
5. Enjoy!!!
