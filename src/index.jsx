import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/App';
import { firebase, config } from './utils/firebase';

firebase.initializeApp(config);

ReactDOM.render(<App />, document.getElementById('app'));

module.hot.accept();
