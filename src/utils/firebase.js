import fire from 'firebase';

export const config = {
  apiKey: 'AIzaSyDnjJUaJ9zxr98vrlVT6beL1HtLV8Jfv5Q',
  authDomain: 'mainsoft-test-task.firebaseapp.com',
  databaseURL: 'https://mainsoft-test-task.firebaseio.com',
  projectId: 'mainsoft-test-task',
  storageBucket: 'mainsoft-test-task.appspot.com',
  messagingSenderId: '795770722561'
};

export const firebase = fire;
