import styled from 'styled-components';

const Content = styled.div`
  height: 100%;
  width: 100%;
  overflow: auto;
  box-sizing: border-box;
  padding: 40px 50px;
  .button-container {
    display: flex;
    justify-content: flex-end;
    align-items: center;
    .progress {
      margin-left: 5px;
    }
  }
  .textField {
    margin: 20px 0;
  }
  .input-block {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 20px 0;
    .textField {
      width: 400px;
    }

    label {
      width: 200px;
    }
  }
`;

export default Content;
