import React from 'react';
import { TextField, InputLabel, Button, CircularProgress } from '@material-ui/core';
import PropTypes from 'prop-types';
import ContentStyled from './ContentStyled';

export default class Content extends React.Component {
  state = {
    isValid: true
  };

  onSave = () => {
    const {
      data: { id, name, details, address, tel, number }
    } = this.props;
    if (name) {
      this.setState({
        isValid: true
      });
      const { onSave } = this.props;
      onSave({ id, name, details, address, tel, number });
    } else {
      this.setState({
        isValid: false
      });
    }
  };

  render() {
    const {
      data: { name, details, address, tel, number },
      handleChange,
      isLoading
    } = this.props;
    const { isValid } = this.state;

    return (
      <ContentStyled>
        <TextField
          error={!isValid}
          className="textField"
          label="Organization name"
          value={name}
          onChange={handleChange('name')}
          fullWidth
        />
        <TextField
          variant="outlined"
          className="textField"
          label="Details"
          value={details}
          onChange={handleChange('details')}
          fullWidth
          multiline
          rows={4}
          rowsMax={10}
        />
        <TextField
          className="textField"
          label="Address"
          value={address}
          onChange={handleChange('address')}
          fullWidth
        />
        <div className="input-block">
          <InputLabel>Phone number</InputLabel>
          <TextField
            className="textField"
            label=""
            value={tel}
            onChange={handleChange('tel')}
            type="tel"
          />
        </div>
        <div className="input-block">
          <InputLabel>Number of employees</InputLabel>
          <TextField
            className="textField"
            label=""
            value={number}
            onChange={handleChange('number')}
            type="number"
          />
        </div>
        <div className="button-container">
          <Button variant="contained" color="primary" onClick={this.onSave}>
            Save
            {isLoading && <CircularProgress className="progress" size={15} color="secondary" />}
          </Button>
        </div>
      </ContentStyled>
    );
  }
}

Content.propTypes = {
  onSave: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  data: PropTypes.oneOfType([
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      details: PropTypes.string.isRequired,
      address: PropTypes.string.isRequired,
      tel: PropTypes.string.isRequired,
      number: PropTypes.number.isRequired
    }),
    PropTypes.bool
  ]).isRequired
};
