import styled from 'styled-components';

const Main = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  overflow: auto;
`;

export default Main;
