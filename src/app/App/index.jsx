import React from 'react';
import uuidv4 from 'uuid/v4';
import { Snackbar } from '@material-ui/core';
import MainStyled from './MainStyled';
import Sidebar from '../Sidebar';
import Content from '../Content';
import { firebase } from '../../utils/firebase';
import GlobalStyle from '../../utils/reset';

export default class App extends React.Component {
  state = {
    selectedOrganization: false,
    organizations: [],
    isLoading: false,
    isLoaded: false
  };

  componentDidMount() {
    this.loadOrganizations();
  }

  loadOrganizations = () => {
    const { database } = firebase;
    database()
      .ref(`organizations/`)
      .on('value', snapshot => {
        let data = [];
        if (snapshot.exists()) {
          const dataMap = Object.entries(snapshot.val());
          data = dataMap.reduce((arr, elem) => {
            arr.push(Object.assign(elem[1], { id: elem[0] }));
            return arr;
          }, []);
        }
        this.setState({
          organizations: data
        });
      });
  };

  loadOrganization = id => {
    const { database } = firebase;
    database()
      .ref(`organizations/${id}`)
      .once('value', snapshot => {
        this.setState({
          selectedOrganization: Object.assign(snapshot.val(), { id })
        });
      });
  };

  onSave = data => {
    const { database } = firebase;
    const { id, name, details, address, tel, number } = data;

    this.setState({
      isLoading: true
    });
    database()
      .ref(`organizations/${id === 'new' ? uuidv4() : id}`)
      .set({
        name,
        details,
        address,
        tel,
        number
      })
      .then(() =>
        this.setState({
          isLoading: false,
          isLoaded: true,
          selectedOrganization: false
        })
      );
  };

  onCreate = () => {
    this.setState({
      selectedOrganization: {
        id: 'new',
        name: '',
        details: '',
        address: '',
        tel: '',
        number: 0
      }
    });
  };

  handleChange = name => event => {
    let { selectedOrganization } = this.state;
    if (name === 'number') {
      selectedOrganization = Object.assign(selectedOrganization, { [name]: +event.target.value });
    } else {
      selectedOrganization = Object.assign(selectedOrganization, { [name]: event.target.value });
    }
    this.setState({
      selectedOrganization
    });
  };

  handleCloseToast = () => {
    this.setState({
      isLoaded: false
    });
  };

  removeOrganization = () => {
    const { database } = firebase;
    const { selectedOrganization } = this.state;
    database()
      .ref(`organizations/${selectedOrganization.id}`)
      .set(null)
      .then(() =>
        this.setState({
          isLoaded: true,
          selectedOrganization: false
        })
      );
  };

  render() {
    const { selectedOrganization, organizations, isLoading, isLoaded } = this.state;
    return (
      <MainStyled>
        <GlobalStyle />
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}
          message="Success"
          open={isLoaded}
          autoHideDuration={3000}
          onClose={this.handleCloseToast}
        />
        <Sidebar
          newOrganization={this.onCreate}
          organizations={organizations}
          loadOrganization={this.loadOrganization}
          selectedOrganization={selectedOrganization}
          removeOrganization={this.removeOrganization}
        />
        {selectedOrganization && (
          <Content
            data={selectedOrganization}
            onSave={this.onSave}
            handleChange={this.handleChange}
            isLoading={isLoading}
          />
        )}
      </MainStyled>
    );
  }
}
