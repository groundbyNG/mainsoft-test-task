import React from 'react';
import { Button, List, ListItem, ListItemText } from '@material-ui/core';
import PropTypes from 'prop-types';
import SidebarStyled from './SidebarStyled';

export default class Sidebar extends React.PureComponent {
  onCreate = () => {
    const { newOrganization } = this.props;
    newOrganization();
  };

  onRemove = () => {
    const { removeOrganization } = this.props;
    removeOrganization();
  };

  renderOrganizations = organizations => {
    const { loadOrganization, selectedOrganization } = this.props;
    return (
      organizations &&
      organizations.map(org => (
        <ListItem
          key={org.id}
          button
          className={selectedOrganization && selectedOrganization.id === org.id ? 'selected' : ''}
          component="button"
          onClick={() => loadOrganization(org.id)}
        >
          <ListItemText className="item-text" primary={org.name} />
        </ListItem>
      ))
    );
  };

  render() {
    const { organizations, selectedOrganization } = this.props;
    return (
      <SidebarStyled>
        <div className="buttons-container">
          <Button onClick={this.onCreate} variant="contained" color="primary" className="create">
            Create
          </Button>
          <Button
            disabled={!selectedOrganization}
            variant="contained"
            color="secondary"
            className="remove"
            onClick={this.onRemove}
          >
            Remove
          </Button>
        </div>
        <List component="nav">{this.renderOrganizations(organizations)}</List>
      </SidebarStyled>
    );
  }
}

Sidebar.propTypes = {
  newOrganization: PropTypes.func.isRequired,
  removeOrganization: PropTypes.func.isRequired,
  organizations: PropTypes.arrayOf(PropTypes.shape).isRequired,
  loadOrganization: PropTypes.func.isRequired,
  selectedOrganization: PropTypes.oneOfType([
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      details: PropTypes.string.isRequired,
      address: PropTypes.string.isRequired,
      tel: PropTypes.string.isRequired,
      number: PropTypes.number.isRequired
    }),
    PropTypes.bool
  ]).isRequired
};
