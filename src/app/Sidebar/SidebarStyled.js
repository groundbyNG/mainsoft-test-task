import styled from 'styled-components';

const Sidebar = styled.div`
  height: 100%;
  width: 350px;
  min-width: 350px;
  overflow: auto;
  border-right: 2px solid;
  box-shadow: -6px 0px 14px 6px;
  .buttons-container {
    border-bottom: 1px solid;
    display: flex;
    button {
      flex: 1;
      border-radius: 0;
    }
  }
  .item-text {
    text-align: center;
  }
  .selected {
    background-color: rgba(0, 0, 0, 0.08);
  }
`;

export default Sidebar;
